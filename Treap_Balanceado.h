//
// Created by Francisco on 21-06-2015.
// Se implementa un Treap con el fin de almacenar palabras sin repetici�n, que van aumentando su priorida a medida
// que aparecen en cada archivo de texto.
//

#ifndef TAREA_RECUPERATIVA_TREAP_BALANCEADO_H
#define TAREA_RECUPERATIVA_TREAP_BALANCEADO_H

#include <iostream>
#include <queue>
#include <string>
#include "Node.h"
#include "Lista_archivos.h"

using namespace std;

class Treap{

public:
    Node* m_Root;
    Node* m_subTree;
    Treap() : m_Root(nullptr), m_subTree(nullptr) { }

    virtual ~Treap() { }

    Node* rotacionPriorDer(Node* sub_root){

        Node* aux = sub_root->m_left;
        sub_root->m_left = aux->m_right;
        aux->m_right = sub_root;
        return aux;
    }

    Node* rotacionPriorIzq(Node* sub_root){
        Node* aux = sub_root->m_right;
        sub_root->m_right = aux->m_left;
        aux->m_left = sub_root;
        return aux;
    }

    void insert(string key,int value){

        m_Root = Insert(m_Root,key,value,0);
    }
    Node* Insert(Node* auxRoot,string key, int value,int priority){             // En esta seccion se inserta una palabra
                                                                                // Hasta el final del arbol y luego seg�n
        if(auxRoot == nullptr){                                                 // la priorida se ocupan rotaciones para
            Node* temp = new Node(key, value, priority);                        // subir el nodo a donde le corresponda
            temp->insert(value);
            return temp;
        }else{
            if(key == auxRoot->key){
                auxRoot->priority = auxRoot->priority + 1;
                auxRoot->insert(value);
                return auxRoot;

            }else if(key < auxRoot->key) {
                auxRoot->m_left = Insert(auxRoot->m_left, key, value,priority);
                if(priority > auxRoot->priority) {
                    auxRoot = rotacionPriorDer(auxRoot);
                }
            }else{
                auxRoot->m_right = Insert(auxRoot->m_right,key,value,priority);
                if(priority > auxRoot->priority){
                    auxRoot = rotacionPriorIzq(auxRoot);
                }
            }
        }

        return auxRoot;
    }

    void Search(string key){                                            //Se retorna la lista de archivos que contienen la palabra

        if(m_Root == nullptr) {
            cout << "Treap Vacio" << endl;
        }else{

            Node* aux = m_Root;
            while(aux != nullptr){
                if(aux->key == key){
                    aux->Lista.Print_List();
                }else if(key < aux->key){
                    aux = aux->m_left;
                    continue;
                }else{
                    aux = aux->m_right;
                    continue;
                }
            }
        }
    }
};

#endif //TAREA_RECUPERATIVA_TREAP_BALANCEADO_H
