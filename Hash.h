//
// Created by Francisco on 27-06-2015.
// Se implemento un diccionario por medio de unordered_map el cual utiliza una tabla hash para insertar elementos
// la Key esta compuesta por la palabra y el value por una lista que almacena los nombres de los archivos en los cuales
// se almacenan.
//

#ifndef TAREA_RECUPERATIVA_HASH_H
#define TAREA_RECUPERATIVA_HASH_H

#include <iostream>
#include <unordered_map>
#include "Lista_archivos.h"
 using namespace std;

class Diccionario_Hash{

public:
    unordered_map<string,Lista_archivos*> Diccionario;
    Diccionario_Hash() {}

    bool buscar(string busqueda){

        unordered_map<string,Lista_archivos*>::const_iterator it = Diccionario.find(busqueda);
        return !(it == Diccionario.end());
    }
    void insert(string word,int Archivo){

        if(!buscar(word)){
            Diccionario.insert(make_pair(word,new Lista_archivos()));
            Diccionario[word]->Insert(Archivo);
        }else{
            auto it = Diccionario.begin();
            while(true){
                if(it->first == word){
                    it->second->Insert(Archivo);
                    break;
                }else{
                    ++it;
                }
            }
        }
    }
    void search(string word){
        auto it = Diccionario.begin();
        while(true){
            if(it->first == word){
                cout << endl;
                it->second->Print_List();
                break;
            }else{
                ++it;
            }
        }
    }
};
#endif //TAREA_RECUPERATIVA_HASH_H
