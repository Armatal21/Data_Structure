//
// Created by Francisco on 21-06-2015.
//

#ifndef TAREA_RECUPERATIVA_NODE_H
#define TAREA_RECUPERATIVA_NODE_H

#include <iostream>
#include "Lista_archivos.h"

using namespace std;

class Node {

public:
    string key;
    int value;
    Lista_archivos Lista ;
    int priority;
    Node*m_right;
    Node*m_left;


    Node(string key,int value, int priority) : key(key), value(value),priority(priority),
                                                             m_right(nullptr),
                                                             m_left(nullptr) {

    }

    virtual ~Node() { }

    void insert(int value){
        Lista.Insert(value);
    }

};
#endif //TAREA_RECUPERATIVA_NODE_H
