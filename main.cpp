// Se reutilizo codigo proporcionado en la tarea para separar palabras y se utiliz� la libreria dirent para obtener
// los nombres de los archivos a los cuales se requirio acceder

#include <iostream>
#include <dirent.h>
#include "Indice_Invertido.h"
#include "Treap_Balanceado.h"
#include "Hash.h"
using namespace std;


int main() {

//    Insercion y busqueda Indice Invertido con Treap

    IndiceInvertido<Treap>* IndiceTreap = new IndiceInvertido<Treap>();
    int cont = 0;
    DIR *pdir = NULL;
    pdir = opendir ("C:\\Users\\Francisco\\Desktop\\wikipedia_40k");
    struct dirent *pent = NULL;

    if (pdir == NULL){
        cout << "\nERROR! pdir could not be initialised correctly";
        exit(3);
    }
    while(pent = readdir(pdir)){

        if(pent == NULL){

            cout << "\nERROR! dir could not be initialised correctly";
            exit(3);
        }
        string Nombre_Archivo = pent->d_name;
        cont++;
        if(cont > 2){

            ifstream ficheroEntrada;
            string texto;
            ficheroEntrada.open ("C:\\Users\\Francisco\\Desktop\\wikipedia_40k\\"+Nombre_Archivo);
            getline(ficheroEntrada,texto);
            ficheroEntrada.close();
            string delimiter = " ";
            int pos = 0;
            string token;
            while ((pos = texto.find(delimiter)) != string::npos)
            {
                token = texto.substr(0, pos);
                texto.erase(0, pos + delimiter.length());
                IndiceTreap->insert(token,atoi(Nombre_Archivo.c_str()));
            }
            IndiceTreap->insert(texto,atoi(Nombre_Archivo.c_str()));
        }

    }
    closedir(pdir);

//    ---------------------------------------------------------------------------------------------------------------
//    ---------------------------------------------------------------------------------------------------------------

//    Insercion Indice Invertido con Hash implementado por medio de unordered_map

    IndiceInvertido<Diccionario_Hash>* IndiceHash = new IndiceInvertido<Diccionario_Hash>();
    int cont2 = 0;
    DIR *dir = NULL;
    dir = opendir ("C:\\Users\\Francisco\\Desktop\\wikipedia_40k");
    struct dirent *dpent = NULL;

    if (dir == NULL){
        cout << "\nERROR! pdir could not be initialised correctly";
        exit(3);
    }
    while(dpent = readdir(dir)){

        if(dpent == NULL){

            cout << "\nERROR! dir could not be initialised correctly";
            exit(3);
        }
        string Nombre_Archivo = dpent->d_name;
        cont2++;
        if(cont2 > 2){

            ifstream ficheroEntrada;
            string texto;
            ficheroEntrada.open ("C:\\Users\\Francisco\\Desktop\\wikipedia_40k\\"+Nombre_Archivo);
            getline(ficheroEntrada,texto);
            ficheroEntrada.close();
            string delimiter = " ";
            int pos = 0;
            string token;
            while ((pos = texto.find(delimiter)) != string::npos)
            {
                token = texto.substr(0, pos);
                texto.erase(0, pos + delimiter.length());
                IndiceHash->insert(token,atoi(Nombre_Archivo.c_str()));
            }
            IndiceHash->insert(texto,atoi(Nombre_Archivo.c_str()));
        }
    }
    closedir(dir);
    delete IndiceHash;
    delete IndiceTreap;

//    ---------------------------------------------------------------------------------------------------------------
//    ---------------------------------------------------------------------------------------------------------------

}