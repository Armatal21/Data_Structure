//
// Created by Francisco on 21-06-2015.
//

#ifndef TAREA_RECUPERATIVA_LISTA_ARCHIVOS_H
#define TAREA_RECUPERATIVA_LISTA_ARCHIVOS_H

#include <iostream>
#include <vector>
using namespace std;

//class Lista_archivos{
//
//public:
//    vector<string> Archivos;
//    Lista_archivos() {}
//
//    void Insetar_lista(string Nombre_archivo){
//
//        Archivos.push_back(Nombre_archivo);
//
//    }
//
//
//};

class Nodo{

public:
    int archivo;
    Nodo* m_next;

    Nodo(int archivo) : archivo(archivo), m_next(nullptr) { }
};

class Lista_archivos{

public:
    Nodo* m_last;
    Nodo* m_head;
    Lista_archivos() : m_last(NULL), m_head(NULL){}

    void Insert(int archivo){

        if(m_last == NULL){
            Nodo* temp = new Nodo(archivo);
            m_head = m_last = temp;
        }else{
            Nodo* temp = new Nodo(archivo);
            if(m_last->archivo != archivo){
                m_last->m_next = temp;
                m_last = temp;
            }

        }
    }

    void Print_List(){

        Nodo* aux = m_head;
        while(aux != nullptr){
            cout << aux->archivo << ", ";
            aux  = aux->m_next;
        }
    }

};

#endif //TAREA_RECUPERATIVA_LISTA_ARCHIVOS_H
