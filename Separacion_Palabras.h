//
// Created by Francisco on 24-06-2015.
//
#ifndef TAREA_RECUPERATIVA_SEPARACION_PALABRAS_H
#define TAREA_RECUPERATIVA_SEPARACION_PALABRAS_H

#include <iostream>
#include <fstream>
#include <string.h>
#include <dirent.h>
#include <string>
#include "Treap_Balanceado.h"
#include "Lectura_archivos.h"

using namespace std;

void Separacion(){

    Treap* Arbol = new Treap();
    int cont = 0;
    DIR *pdir = NULL;
    pdir = opendir ("C:\\Users\\Francisco\\Documents\\Cursos\\14 - Estructura de Datos\\Tarea Recuperativa\\Archivos\\cosa");
    struct dirent *pent = NULL;

    if (pdir == NULL){
        cout << "\nERROR! pdir could not be initialised correctly";
        exit(3);
    }
    while(pent = readdir(pdir)){

        if(pent == NULL){

            cout << "\nERROR! dir could not be initialised correctly";
            exit(3);
        }
        string Nombre_Archivo = pent->d_name;
        cont++;
        if(cont > 2){

            ifstream ficheroEntrada;
            string texto;
            ficheroEntrada.open ("C:\\Users\\Francisco\\Documents\\Cursos\\14 - Estructura de Datos\\Tarea Recuperativa\\Archivos\\cosa\\"+Nombre_Archivo);
            getline(ficheroEntrada,texto);
            ficheroEntrada.close();
            //cout << texto << endl;
            string delimiter = " ";
            int pos = 0;
            string token;
            while ((pos = texto.find(delimiter)) != string::npos)
            {
                token = texto.substr(0, pos);
                cout << token << endl;
                texto.erase(0, pos + delimiter.length());
                Arbol->insert(Arbol->auxRoot,token,Nombre_Archivo,0);

            }
            Arbol->insert(Arbol->auxRoot,token,Nombre_Archivo,0);
//            cout << texto << endl;

        }
    }
    closedir(pdir);
}

#endif //TAREA_RECUPERATIVA_SEPARACION_PALABRAS_H
