//
// Created by Francisco on 23-06-2015.
//

#ifndef TAREA_RECUPERATIVA_LECTURA_ARCHIVOS_H
#define TAREA_RECUPERATIVA_LECTURA_ARCHIVOS_H

#include <iostream>
#include <string.h>
#include <dirent.h>

using namespace std;

string Lectura_archivos(){

    int cont = 0;
    DIR *pdir = NULL;
    pdir = opendir ("C:\\Users\\Francisco\\Documents\\Cursos\\14 - Estructura de Datos\\Tarea Recuperativa\\Archivos\\cosa");
    struct dirent *pent = NULL;

    if (pdir == NULL){
        cout << "\nERROR! pdir could not be initialised correctly";
        exit(3);
    }
    while(pent = readdir(pdir)){

        if(pent == NULL){

            cout << "\nERROR! dir could not be initialised correctly";
            exit(3);
        }
        return pent->d_name;
    }

    closedir(pdir);
}

#endif //TAREA_RECUPERATIVA_LECTURA_ARCHIVOS_H
